$('document').ready(function () {
  const totalNumOfClients = document.querySelectorAll('.client-container').length;
  let newClientNum;

  function applyNewClient(newClientNum) {
    $('.client-photo').removeClass('active-client-photo');
    $(`.client-photo[data-photo=${newClientNum}]`).addClass('active-client-photo');
    $('.client-container').hide();
    $(`.client-container[data-box=${newClientNum}]`).fadeIn('slow');
  }

  function chooseClient() {
    newClientNum = this.dataset.photo;
    applyNewClient(newClientNum);
  }

  function nextClient() {
    const activeClientNum = +document.querySelector('.active-client-photo').dataset.photo;

    if (activeClientNum < totalNumOfClients) {
      newClientNum = activeClientNum + 1;
    } else {
      newClientNum = 1;
    }
    applyNewClient(newClientNum);
  }

  function previousClient() {
    const activeClientNum = +document.querySelector('.active-client-photo').dataset.photo;

    if (activeClientNum > 1) {
      newClientNum = activeClientNum - 1;
    } else {
      newClientNum = totalNumOfClients;
    }
    applyNewClient(newClientNum);
  }

  $('.client-photo').on('click', chooseClient);
  $('.client-slider-button.client-prev').on('click', previousClient);
  $('.client-slider-button.client-next').on('click', nextClient);
});
