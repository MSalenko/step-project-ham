const galleryBtn = document.getElementById('gallery-btn');
const gallerySpinner = document.getElementById('gallery-spinner');
const galleryImg = document.querySelectorAll('.gallery-img');

function showMoreImg() {
    galleryImg.forEach(img => {
        img.classList.add('gallery-img-active');
    });
    galleryBtn.style.display = 'none';
  }
  
function delayImgDownload() {
    setTimeout(showMoreImg, 2000);
    gallerySpinner.style.display = 'block';
    setTimeout(() => {
    gallerySpinner.style.display = 'none';
    }, 2000);
  }

  galleryBtn.addEventListener('click', delayImgDownload);