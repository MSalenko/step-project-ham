const serviceMenu = document.querySelector('.service-menu');
const serviceText = document.querySelectorAll('.s-menu-text');
const serviceImage = document.querySelectorAll('.s-menu-image');
const serviceTriangle = document.querySelectorAll('.t-box');

function showServiceContent(item) {
    for (const menuItem of serviceMenu.children) {
        menuItem.classList.remove('active-s-menu-item');
    }
    item.target.classList.add('active-s-menu-item');

    const activeItem = item.target.dataset.menu;

    serviceImage.forEach(img => {
        img.classList.remove('image-active');
        if (img.dataset.img === activeItem) {
            img.classList.add('image-active');
        }
    });

    serviceText.forEach(text => {
        text.classList.remove('text-active');
        if (text.dataset.text === activeItem) {
        text.classList.add('text-active');
        }
    });
    serviceTriangle.forEach(tBox => {
        tBox.classList.remove('t-box-active');
        if (tBox.dataset.tbox === activeItem) {
        tBox.classList.add('t-box-active');
        }
    });
  }
  serviceMenu.addEventListener('click', showServiceContent);