const workMenu = document.querySelector('.work-list');
const workItemList = document.querySelectorAll('.grid-menu-item');
const loadMoreBtn = document.getElementById('load-more-btn');
const spinner = document.getElementById('work-spinner');
let activeItemsNumber = 12;

function showWorkContent(item) {
  for (const menuItem of workMenu.children) {
    menuItem.classList.remove('list-item-active');
  }
  item.target.classList.add('list-item-active');

  workItemList.forEach(el => {
    el.classList.add('display-none');
  });

  if (item.target.dataset.id === 'all') {
    for (let i = 0; i < activeItemsNumber; i++) {
      workItemList[i].classList.remove('display-none');
    }
    hideLoadMoreButton();
  } else {
    let i = 0;
    workItemList.forEach(el => {
      if (el.classList.contains(`${item.target.dataset.id}`)) {
        el.classList.remove('display-none');
        i++;
      }
    });
    if (i <= activeItemsNumber) {
      loadMoreBtn.style.display = 'none';
    }
  }
}
function showMoreContent() {
  activeItemsNumber += 12;
  for (let i = 0; i < activeItemsNumber; i++) {
    workItemList[i].classList.remove('display-none');
  }
  hideLoadMoreButton();
}

function hideLoadMoreButton() {
  if (activeItemsNumber >= workItemList.length) {
    loadMoreBtn.style.display = 'none';
  } else {
    loadMoreBtn.style.display = 'flex';
  }
}

function delayDownload() {
  setTimeout(showMoreContent, 2000);
  spinner.style.display = 'block';
  setTimeout(() => {
    spinner.style.display = 'none';
  }, 2000);
}

workMenu.addEventListener('click', showWorkContent);
loadMoreBtn.addEventListener('click', delayDownload);

// grid-menu-item animation:
$('.grid-menu-item')
  .mouseenter(function () {
    $(this.children[0]).hide();
    $(this.children[1]).fadeIn('fast');
  })
  .mouseleave(function () {
    $(this.children[1]).hide();
    $(this.children[0]).fadeIn('slow');
  });
